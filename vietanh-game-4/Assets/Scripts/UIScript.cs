﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIScript : MonoBehaviour
{
    PlayerScript end;
    private bool isClicked = false;
    public GameObject text;
    // Start is called before the first frame update
    public void Pause()
    {
        if (isClicked == false)
        {
            isClicked = true;
            Time.timeScale = 0.0f;
            text.SetActive(true);
        }
        else if(isClicked==true) { isClicked = false; Time.timeScale = 1.0f;text.SetActive(false); }
    }
}
