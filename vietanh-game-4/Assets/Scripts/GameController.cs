﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public PlayerController playerController;
    public LayerMask layer;
    public Rigidbody player;
    public Rigidbody enemy;

    public Transform end;
    public float speed;

    bool isClicked;
    // Start is called before the first frame update
    void Start()
    {
        transform.Rotate(Vector3.right, 60);
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit[] hits;
        List<GameObject> list = new List<GameObject>();

        if (Input.GetButtonDown("Fire1") && (int)playerController.playerState == 1)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            hits = Physics.RaycastAll(ray);
            for (int i = 0; i < hits.Length; i++)
            {
                RaycastHit hit = hits[i];
                if (hit.collider.gameObject.tag == "Player")
                {
                    isClicked = true;
                }
                else isClicked = false;
            }
        }
        if (Input.GetButton("Fire1") && (int)playerController.playerState == 1)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            hits = Physics.RaycastAll(ray);
            for (int i = 0; i < hits.Length; i++)
            {
                RaycastHit hit = hits[i];
                if (hit.collider.gameObject.tag == "Player")
                {
                    isClicked = true;
                }
                if (hit.collider.gameObject.tag == "Plane" && isClicked)
                {
                    player.position = hit.point;
                }
                else if (hit.collider.gameObject.tag == "Plane")
                {
                    player.velocity = (end.position - player.position).normalized * Time.deltaTime * speed;
                }

            }
        }

    }
}
