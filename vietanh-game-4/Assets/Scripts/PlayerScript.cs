﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerScript : MonoBehaviour
{
    public GameObject end_text;
    public GameObject pause_text;

    public static bool isEnd = false;
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "End"||collision.gameObject.tag == "Enemy")
        {
            end_text.SetActive(true);
            pause_text.SetActive(false);

            Time.timeScale = 0.0f;
            isEnd = true;
        }
    }
}
