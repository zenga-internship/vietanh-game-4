﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public enum EnemyState
    {
        Idle,
        Danger
    }

    private EnemyState enemyState;
    public Rigidbody enemy;
    public Vector3 start, end;
    public float speed;
    public float delay;

    public Material[] materials;
    IEnumerator Start()
    {
        enemyState = EnemyState.Idle;
        //Debug.Log(enemyState);
        while (true)
        {
            switch (enemyState)
            {
                case EnemyState.Idle:
                    {
                        //Debug.Log(enemyState);
                        Idle();
                        yield return new WaitForSeconds(delay);
                        enemyState = EnemyState.Danger;
                        break;
                    }
                case EnemyState.Danger:
                    {
                        //Debug.Log(enemyState);
                        Danger();
                        yield return new WaitForSeconds(delay);
                        enemyState = EnemyState.Idle;
                        break;
                    }
            }
            yield return 0;
        }

    }
    void Idle()
    {
        enemy.velocity = Vector3.zero;
        MeshRenderer enemyMat = enemy.GetComponent<MeshRenderer>();
        enemyMat.material=materials[(int)enemyState];
    }
    void Danger()
    {
        //enemy.AddForce(new Vector3(0,1,0),ForceMode.Impulse);
        if (enemy.position.x > end.x) speed *= -1;
        if (enemy.position.x < start.x) speed *= -1;
        //enemy.velocity = Vector3.right * speed * Time.deltaTime;
        enemy.velocity=Vector3.right*speed*Vector3.Distance(start,end)*Time.deltaTime;

        MeshRenderer enemyMat = enemy.GetComponent<MeshRenderer>();
        enemyMat.material=materials[(int)enemyState];
    }
}
