﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public enum PlayerState
    {
        Idle,
        Active
    }

    public PlayerState playerState;
    public Rigidbody player;

    public float speed;
    public Material[] materials;
    IEnumerator Start()
    {
        playerState = PlayerState.Idle;
        //Debug.Log(playerState);
        while (true)
        {
          switch (playerState)
          {
              case PlayerState.Idle:
                  {
                      //Debug.Log(playerState);
                      Idle();
                      yield return new WaitForSeconds(1.5f);
                      playerState = PlayerState.Active;
                      break;
                  }
              case PlayerState.Active:
                  {
                      //Debug.Log(playerState);
                      Active();
                      yield return new WaitForSeconds(2.0f);
                      playerState = PlayerState.Idle;
                      break;
                  }
          }
          yield return 0;
        }

    }
    void Idle()
    {
        Debug.Log("jump");
        if (player.position.y < 0.56)
          player.AddForce(new Vector3(0, 5, 0), ForceMode.Impulse);
          //player.AddForce(Vector3.up * Time.deltaTime * speed, ForceMode.Impulse);
        MeshRenderer enemyMat = player.GetComponent<MeshRenderer>();
        enemyMat.material = materials[(int)playerState];

    }
    void Active()
    {
        //Debug.Log("moveable");
        MeshRenderer enemyMat = player.GetComponent<MeshRenderer>();
        enemyMat.material = materials[(int)playerState];
    }
}
