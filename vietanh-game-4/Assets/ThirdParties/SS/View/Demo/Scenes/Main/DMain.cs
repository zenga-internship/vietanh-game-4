﻿using UnityEngine;
using SS.View;
using System.Collections;

public class DMain : MonoBehaviour
{
    IEnumerator Start()
    {
        //coroutine
        //wait 1s
        yield return new WaitForSeconds(1);
        //load scene name
        Manager.LoadingSceneName = DLoadingController.SCENE_NAME;
        //load scene
        Manager.Load(DTopController.SCENE_NAME);
    }
}
