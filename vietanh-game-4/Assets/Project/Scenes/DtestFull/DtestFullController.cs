using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SS.View;

public class DtestFullController : Controller
{
    public const string DTESTFULL_SCENE_NAME = "DtestFull";

    public override string SceneName()
    {
        return DTESTFULL_SCENE_NAME;
    }
    public void OnLoadNonFullScene()
    {
        Manager.Load(DtestNonFullController.DTESTNONFULL_SCENE_NAME);
    }
    public void OnLoadPopupScene()
    {
        Manager.Add(PopupController.SCENE_NAME, "POPUP nao do");
    }
}