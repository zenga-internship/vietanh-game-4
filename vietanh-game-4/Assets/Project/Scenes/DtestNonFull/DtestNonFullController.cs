using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SS.View;

public class DtestNonFullController : Controller
{
    public const string DTESTNONFULL_SCENE_NAME = "DtestNonFull";

    public override string SceneName()
    {
        return DTESTNONFULL_SCENE_NAME;
    }
    public void Back(){
        Manager.Load(DtestFullController.DTESTFULL_SCENE_NAME);
    }
}